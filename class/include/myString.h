#ifndef MYSTRING_H
#define MYSTRING_H

class MyString{

      public:
      
            // const
            MyString();
            MyString(std::string input);

            // dest
            virtual ~MyString();

            // Liste des méthodes comme dans un header normal
            void addBefore(std::string input);
            void addAfter(std::string input);
            void replace(std::string toFind, std::string replaceWith);

            int len();

            // Just returns the string
            std::string toCpp_string();
            char * toC_string(); 

      protected:

      private:
            std::string myString;
            std::ostringstream mySstream;
            char * my_toC_string();

};

#endif // MYSTRING_H
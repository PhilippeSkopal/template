// OSlib header file
#include <oslib/oslib.h>

// basic c libs
#include <stdio.h>
#include <stdlib.h>

// needed for strings
#include <string>
#include <sstream>

// personal classes
#include <class/myString.cpp>
#include <class/controls.cpp>

//Necessary to create eboot
PSP_MODULE_INFO("OSLib Sample", 0, 1, 1);
PSP_MAIN_THREAD_ATTR(THREAD_ATTR_USER | THREAD_ATTR_VFPU);

int main()
{
    //Initialization of the Oslib library
    oslInit(0);

    //Initialization of the graphics mode
    oslInitGfx(OSL_PF_8888, 1);

    //Initialization of the text console
    oslInitConsole();

    //Main loop
    while (!osl_quit)
    {

        std::string str = "esg";
        MyString * string = new MyString(str);
        string->addAfter(" ");
        string->addAfter(" ");

        //To be able to draw on the screen
        oslStartDrawing();
        
        //initiate the PSP's buttons
        oslReadKeys();
        
        //clears the screen
        oslCls();
  
        oslPrintf_xy(20,20,"What is your favorite psp site?");
        oslPrintf_xy(20,35,"If you like psp-hacks.com, push the X button");
        oslPrintf_xy(20,45,"If you like psp-downgrades.com, push the square button");
        oslPrintf_xy(20,55,"If you like psp-haxors.com, push the circle button");
        oslPrintf_xy(20,65,"If you like zingaburga.co.nr, push the triangle button");
        oslPrintf_xy(20,75, string->toC_string());

        delete string;
         
        if (osl_keys->held.cross) oslPrintf_xy(20,85,"You voted for greg's site");
        if (osl_keys->held.square) oslPrintf_xy(20,85,"You voted for power's site");
        if (osl_keys->held.circle) oslPrintf_xy(20,85,"You voted for Chaos_Zero's site");
        if (osl_keys->held.triangle) oslPrintf_xy(20,85,"You voted for ZingaBurga's site");

        oslPrintf_xy(20,100,"Value of joystick X : %d",osl_keys->analogX);
        oslPrintf_xy(20,110,"Value of joystick Y : %d",osl_keys->analogY);

        //Ends drawing mode
        oslEndDrawing();

        //Synchronizes the screen 
        oslSyncFrame();        
    }
    
    //Terminate the program
    oslEndGfx();
    oslQuit();
    return 0;
}